import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-detector-layer',
  templateUrl: './detector-layer.component.html',
  styleUrls: ['./detector-layer.component.css']
})
export class DetectorLayerComponent implements OnInit {

  @Input() detectors;

  constructor() { }

  ngOnInit() {
  }

}
