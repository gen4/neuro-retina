import { Injectable } from '@angular/core';
import detectorsData from '../assets/detectorsData_12';
@Injectable()
export class ConfigService {

  detectors = detectorsData;
  imageUrl = '../assets/images/zebra.jpg';
  width = 1024;
  height = 683;
  
  constructor() {  }

}
