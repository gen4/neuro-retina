import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { DetectorLayerComponent } from './detector-layer/detector-layer.component';
import {ConfigService} from "./config.service";
import { ImageLayerComponent } from './image-layer/image-layer.component';

@NgModule({
  declarations: [
    AppComponent,
    DetectorLayerComponent,
    ImageLayerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [{provide:'Config',useClass:ConfigService}],
  bootstrap: [AppComponent]
})
export class AppModule { }
