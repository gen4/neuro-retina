import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image-layer',
  templateUrl: './image-layer.component.html',
  styleUrls: ['./image-layer.component.css']
})
export class ImageLayerComponent implements OnInit {

  @Input() imageUrl;
  image = new Image();

  constructor() {
    this.image.src = '../assets/images/MorePerfect.png';
  }

  ngOnInit() {
  }

}
