import { NeuroRetinaPage } from './app.po';

describe('neuro-retina App', function() {
  let page: NeuroRetinaPage;

  beforeEach(() => {
    page = new NeuroRetinaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
